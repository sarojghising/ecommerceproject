@extends('layouts.master')
@section('title','Shop Page |Bcom, an online store')
@section('meta')
    <meta name="keyword" content="online shop, ecommerce, nepali ecommerce, online shopping, shopping cart, kathmandu, Mt.Everest, everest">
    <meta name="description" content="First online nepali ecommerce portal providing you everthing you need">
    <meta name="author" content="Admin Bcom">

    <meta property="og:url" content="{{ route('all-product-list') }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Home Page |Bcom, an online store">
    <meta property="og:description" content="First online nepali ecommerce portal providing you everthing you need">
    <meta property="og:image" content="{{ asset('images/icons/logo-01.png') }}">
@endsection

@section('main-content')
    <!-- Product -->
    <section class="bg0 p-t-23 p-b-140">
        <div class="container">
            <div class="p-b-10">
                <h3 class="ltext-103 cl5">
                    All Products
                </h3>
            </div>

            <div class="flex-w flex-sb-m p-b-52">
                <div class="flex-w flex-l-m filter-tope-group m-tb-10">
                    <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
                        All Products
                    </button>

                    @if(isset($categories))
                        @foreach($categories as $cat_id => $cat_data)
                            <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".{{ $cat_id }}">
                                {{ $cat_data }}
                            </button>
                        @endforeach
                    @endif
                </div>

                <div class="flex-w flex-c-m m-tb-10">

                    <div class="flex-c-m stext-106 cl6 size-105 bor4 pointer hov-btn3 trans-04 m-tb-4 js-show-search">
                        <i class="icon-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-search"></i>
                        <i class="icon-close-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-close dis-none"></i>
                        Search
                    </div>
                </div>

                <!-- Search product -->
                <div class="dis-none panel-search w-full p-t-10 p-b-15">
                    <div class="bor8 dis-flex p-l-15">
                        <button class="size-113 flex-c-m fs-16 cl2 hov-cl1 trans-04">
                            <i class="zmdi zmdi-search"></i>
                        </button>

                        <input class="mtext-107 cl2 size-114 plh2 p-r-15" type="text" name="search-product"
                               placeholder="Search">
                    </div>
                </div>


            </div>

            @include('home.section.product-list')
        </div>
    </section>
@endsection

