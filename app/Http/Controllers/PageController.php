<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
     protected $page = null;
     public function __construct(Page $page)
     {
         $this->page = $page;
     }

    public function index()
    {
        $data = $this->page->get();
        return view('admin.pages')->with('page_list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->page = $this->page->find($id);
        if (!$this->page){
            request()->session()->flash('error','id not found');
            return redirect()->route('pages.index');
        }

        return view('admin.pages-form')
            ->with('page_data',$this->page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->page = $this->page->find($id);
        if (!$this->page){
            request()->session()->flash('error','id not found');
            return redirect()->route('pages.index');
        }

        $data = $request->all();
        if ($request->has(('image'))){
            $pages_images = uploadImage($request->image,'pages','700x700');
            if ($pages_images){
                $data['image'] = $pages_images;
            }
        }
        $data['description'] = htmlentities($request->description);
        //dd($data);
        $this->page->fill($data);
        $success = $this->page->save();
        if ($success){
            $request->session()->flash('success','Updated Successfully');
        }else{
            $request->session()->flash('error','There was problem while updating Pages');

        }

        return redirect()->route('pages.index');

    }
    public function destroy($id)
    {
        //
    }
    public function getHelpAndFaq(Request $request){
        $this->page = $this->page->where('slug','help-and-faq')->first();
        if (!$this->page){
            abort(404);
        }
        return view('home.page')->with('page_content',$this->page);
    }
}
