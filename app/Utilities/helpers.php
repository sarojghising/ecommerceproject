<?php



//function

function uploadImage($file,$dir,$thumb_dimension = null) // 1920 x 768
{
    $path = public_path().'/uploads/'.$dir;
if (!File::exists($path)){
    File::makeDirectory($path,0777,true,true);
}

$file_name = ucfirst($dir).'-'.date('Ymdhis').rand(0,999).".".$file->getClientOriginalExtension();
$uploads  = $file->move($path,$file_name);
if ($uploads){
  $file_path = $path.'/'.$file_name;
  if ($thumb_dimension){
      list($width,$height) = explode('x',$thumb_dimension);
        Image::make($file_path)->resize($width,$height,function ($constraint){
            $constraint->aspectRatio();
        })->save($path.'/Thumb-'.$file_name);
  }
  return $file_name;
  }else{
    return null;
}
}


function getCategoryMenu(){
    $category = new \App\Models\Category();
    $all_parent_cats = $category->getAllCategoryForMenu();
    if($all_parent_cats){
        echo  '<ul class="dropdown" style="width:250px; ">';
        echo '<li><a href="'.route('all-product-list').'">All Categories</a></li>';
        foreach($all_parent_cats as $parent_categories) {
                if ($parent_categories->child_categories->count() > 0){
                    ?>
                    <li>
                        <a href=" <?php echo route('parent-category-product',$parent_categories->slug) ?> "><?php echo $parent_categories->title; ?></a>
                        <ul class="dropdown-subcontent" style="width: 250px;">
                            <?php
                            foreach ($parent_categories->child_categories as $child_data){

                                echo '<li><a href='.route('child-category-product',[$parent_categories->slug,$child_data->slug]).'>'.$child_data->title.'</a></li>';
                            }

                            ?>
                        </ul>
                    </li>
                <?php

                } else{
                    echo '<li><a href="'.route('parent-category-product',$parent_categories->slug ).'">All Categories</a></li>';
                }

        }

        echo '</ul>';

    }


}

