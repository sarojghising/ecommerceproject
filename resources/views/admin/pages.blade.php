@extends('admin.layouts.admin')
@section('content')
@section('styles')
    {{--        <link rel="stylesheet" href="{{ asset('assets/admin/css/datatable.min.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Pages List</div>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped table-hover" id="table_id">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Thumbnail</th>
                                <th>Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($page_list))
                                @foreach($page_list as $key => $value)
                                    <tr>
                                        <td>{{ $value->title }}</td>
                                        <td>
                                            @if($value->image != null && file_exists(public_path().'/uploads/pages/Thumb-'.$value->image))
                                                <img src="{{ asset('uploads/pages/Thumb-'.$value->image) }}" alt="img" style="max-width: 150px" class="img img-responsive img-thumbnail">
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('pages.show',$value->slug) }}" target="_blank"> {{ route('pages.show',$value->slug) }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ route('pages.edit',$value->id) }}" class="btn btn-success" style="border-radius: 50%">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    {{--    <script rel="stylesheet" href="{{ asset('assets/admin/js/datatable.min.js') }}"></script>--}}
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection
