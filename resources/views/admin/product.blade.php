@extends('admin.layouts.admin')
@section('content')
@section('styles')
    {{--        <link rel="stylesheet" href="{{ asset('assets/admin/css/datatable.min.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Products List</div>
                        <div class="ibox-tools">
                            <a href="{{ route('products.create') }}"><button class="btn btn-success"><i class="fa fa-plus"></i> Add Category</button></a>
                        </div>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped table-hover" id="table_id">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Parent_Category</th>
                                <th>Child_Category</th>
                                <th>Price(NPR.)</th>
                                <th>Discount(%)</th>
                                <th>Brand</th>
                                <th>Is Featured</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($products_list))
                                @foreach($products_list as $key => $value)
                                    <tr>
                                        <td>
                                            <a href="{{  route('products.show',$value->slug) }}" target="_product_detail">
                                                {{$value->title }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $value->cat_info['title'] }}
                                        </td>
                                        <td>
                                            <Strong class="text-success">{{ $value->sub_cat_info['title'] }}</Strong>
                                        </td>
                                        <td>{{ number_format($value->price) }}</td>
                                        <td>{{ number_format($value->discount) }}%</td>
                                        <td>{{ $value->brand }}</td>
                                        <td>{{ $value->is_featured ? 'Yes' : 'No' }}</td>
                                        <td>{{ ucfirst($value->status) }}</td>
                                        <td>
                                            <a href="{{ route('products.edit',$value->id) }}" class="btn btn-success" style="border-radius: 50%">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            {{--delelte data--}}
                                            {{ Form::open(['url' => route('products.destroy',$value->id),'class' => 'form' ,'method' =>'delete','onsubmit' =>"return confirm('Are u sure u want to delete this products.?')"]) }}
                                            {{Form::button('<i class="fa fa-trash"></i>',['class' => 'btn btn-danger','style' => 'border-radius:50% ','type' => 'submit'])}}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    {{--    <script rel="stylesheet" href="{{ asset('assets/admin/js/datatable.min.js') }}"></script>--}}
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection
