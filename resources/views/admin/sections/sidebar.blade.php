<!-- START SIDEBAR-->
<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <img src="{{ asset('images/userinfo.png') }}" width="45px" style="background: #ffffff;">
            </div>
            <div class="admin-info">
                <div class="font-strong">{{ request()->user()->name }}</div><small>Administrator</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="{{ route('admin') }}"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>

            <li class="heading">FEATURES</li>

            <li>
                <a href="{{ route('banner.index') }}"><i class="sidebar-item-icon fa fa-image"></i>
                    <span class="nav-label">Banner Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('category.index') }}"><i class="sidebar-item-icon fa fa-sitemap"></i>
                    <span class="nav-label">Category Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('file-manager') }}"><i class="sidebar-item-icon fa fa-image"></i>
                    <span class="nav-label">Media Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('products.index') }}"><i class="sidebar-item-icon fa fa-shopping-basket"></i>
                    <span class="nav-label">Product Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('order.index') }}"><i class="sidebar-item-icon fa fa-shopping-bag"></i>
                    <span class="nav-label">Order Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('users.index') }}"><i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label">Users Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('pages.index') }}"><i class="sidebar-item-icon fa fa-paperclip"></i>
                    <span class="nav-label">Pages Management</span>
                </a>
            </li>
            <li>
                <a href="icons.html"><i class="sidebar-item-icon fa fa-comments"></i>
                    <span class="nav-label">Review Management</span>
                </a>
            </li>
        </ul>
    </div>
</nav>

<!-- END SIDEBAR-->