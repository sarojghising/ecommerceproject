<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Str;
class Category extends Model
{

    protected $fillable = ['title','summary','is_parent','parent_id','image','status','slug','added_by'];
    public function  getRules($act = 'add'){
        $rules = [
            'title' => 'required|string|unique:categories,title',
            'summary' => 'nullable|string',
            'is_parent'  => 'sometimes|in:1',
            'parent_id' => 'sometimes|exists:categories,id',
            'image' => 'sometimes|image|max:3000',
            'status' => 'required|in:active,inactive'
        ];

        if ($act != 'add'){
            $rules['title'] = 'required|string';
        }
        return $rules;
    }
    public  function  parent_info(){
        return $this->hasOne('App\Models\Category','id','parent_id');

    }
    public  function  getChildCatId($parent_id){
            return $this->where('parent_id',$parent_id)->pluck('id');
    }
    public function getAllCategories(){
        return $this->with('parent_info')->orderBy('id','DESC')->get();
    }
    public function  getAllParents(){
        //select * from category  where is_parent = 1
        //id => title
        return $this->where('is_parent',1)->orderBy('title','ASC')->pluck('title','id');

    }

    public function  getChilds($parent_id){
        return $this->where('parent_id',$parent_id)->orderBy('title','ASC')->pluck('title','id');
    }

    public function child_categories(){
        return $this->hasMany('App\Models\Category','parent_id','id')->where('status','active')->orderBy('title','ASC');
    }

    public function getAllCategoryForMenu(){
        return $this->where('is_parent',1)->with('child_categories')->where('status','active')->orderBy('title','ASC')->get();

    }
    public function getSlug($title){
        $slug = Str::slug($title);
        $exits = $this->where('slug',$slug)->first();
        if ($exits){
            $slug .= date('Ymdhis');
        }
        return $slug;
    }
    public function  products(){
        return $this->hasMany('App\Models\Product','cat_id','id')->where('status','active');
    }
    public function  child_products(){
        return $this->hasMany('App\Models\Product','sub_cat_id','id')->where('status','active');
    }
    public function getCategoryBySlug($slug){
        return $this->with('products')->where('slug',$slug)->first();
    }
    public function getChildCategoryBySlug($slug){
        return $this->with('child_products')->where('slug',$slug)->first();
    }


}
