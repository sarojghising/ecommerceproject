<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        //dd($request->user()->role);

        return redirect()->route($request->user()->role);

//        echo $request->user()->role;
//        return view('home');
        //logic to redirect
        //return view('home');
    }

    public  function  vendor(Request $request){
        if($request->user()->role != 'vendor'){
            $request->session()->flash('status','You Are Not Vendor Sorry !!!');
            return redirect()->route($request->user()->role);
        }
        return view('home');
    }

}
