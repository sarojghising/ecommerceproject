<?php
use Illuminate\Support\Facades\Route;

Route::get('/','FrontendController@index')->name('landing');

Auth::routes(['register' => false,'verify' => true]);

Route::get('/sign-up',function (){
   return view('home.signup');
})->name('user-register');

//checkout and view cart route
Route::get('/cart','ProductsController@showCart')->name('cart-detail');
Route::get('/checkout','ProductsController@checkOut')->name('checkout')->middleware('auth');

//review route define
Route::post('/review/{product_id}','ProductsController@submitReview')->name('post-review');
// cart ajax
Route::get('/cart/add','ProductsController@addToCart');
//email verification
Route::get('/activate/{user_id}/{token}','UsersController@activeUser')->name('user_active');

// resource route
Route::post('/sign-up','UsersController@submitUser')->name('post-user-register');


Route::get('/home', 'HomeController@index')->name('home');

//getHelpAndFaq route
Route::get('/help-and-faq','PageController@getHelpAndFaq')->name('help-and-faq');
//Route::get('/pages/{slug}','PageController@getPageDetail')->name('page-detail');


Route::get('/shop','ProductsController@getAllProducts')->name('all-product-list');

Route::get('/category/{slug}','CategoryController@getProductByPrentCat')->name('parent-category-product');
Route::get('/category/{slug}/{child_slug}','CategoryController@getProductByChildCat')->name('child-category-product');





Route::get('/products/{slug}','ProductsController@show')->name('products-show');

/*------------------------ Admin Route ---------------------------*/
Route::group(['prefix' => 'admin','middleware' => ['auth','admin']],function (){

    Route::get('/',function (){
        return view('admin.dashboard');
    })->name('admin');

    Route::resource('banner','BannerController')->except('show');
    Route::resource('category','CategoryController');
    Route::resource('products','ProductsController');
    Route::resource('users','UsersController');
    Route::resource('pages','PageController');
    Route::resource('order','OrderController');
    Route::post('/category/child','CategoryController@getChildCats')->name('get-child');


    Route::get('file-manager',function (){
        return view('admin.file-manager');
    })->name('file-manager');
});

/*------------------------ End Admin Route ---------------------------*/

/*------------------------ Vendor Route ---------------------------*/
Route::group(['prefix' => 'vendor','middleware' => ['auth','vendor']],function (){
    Route::get('/','HomeController@index')->name('vendor');
});
/*------------------------ End Vendor Route ---------------------------*/

/*------------------------ Customer Route ---------------------------*/
Route::group(['prefix' => 'customer','middleware' => ['auth','customer']],function (){
    Route::get('/',function (){
        return view('home');
    })->name('customer');
});

/*------------------------ End Customer Route ---------------------------*/
Route::group(['middleware' => 'auth'], function () {
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
    // list all lfm routes here...
});
