@extends('admin.layouts.admin')
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/select2.css') }}">
@endsection
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Products{{ isset($product_data) ? 'Update' : 'Add' }}</div>
                    </div>
                    <div class="ibox-body">
                        @if(isset($product_data))
                            {{ Form::open(['url' => route('products.update',$product_data->id),'class' => 'form', 'id' => 'products_add', 'files' => true,'method' => 'patch']) }}
                        @else
                            {{ Form::open(['url' => route('products.store'),'class' => 'form', 'id' => 'category_add', 'files' => true ]) }}
                        @endif
                        <div class="form-group row">
                            {{ Form::label('title','Title: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::text('title',@$product_data->title ,['class' => 'form-control'.($errors->has('title') ? 'is-invalid': ''),'id' => 'title' ,'required' => true,'placeholder' => 'Enter Title']) }}
                                {{--name,value,class--}}
                                @error('title')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('summary','Summary: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::textarea('summary',@$product_data->summary,['class' => 'form-control'.($errors->has('summary') ? 'is-invalid': ''),'id' => 'summary' ,'required' => false,'placeholder' => 'Enter summary','rows' => 5, 'style' => 'resize:none']) }}
                                {{--name,value,class--}}
                                @error('summary')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>
                            <div class="form-group row">
                                {{ Form::label('description','Description: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::textarea('description',@$product_data->description,['class' => 'form-control'.($errors->has('description') ? 'is-invalid': ''),'id' => 'description' ,'placeholder' => 'Enter description','rows' => 5, 'style' => 'resize:none']) }}
                                    {{--name,value,class--}}
                                    @error('description')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>

                        <div class="form-group row">
                            {{ Form::label('cat_id','Category: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::select('cat_id', $parents_cats ,@$product_data->cat_id,['id' => 'cat_id','placeholder'=> '---Select One Category ---','class' => 'form-control form-control-sm select2']) }}
                                {{--name,value,class--}}
                                @error('cat_id')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>


                            <div class="form-group row {{ (isset($child_cats) && ($child_cats->count() > 0 )) ? '' : 'd-none' }}" id="child_cat_div">
                                {{ Form::label('sub_cat_id','Sub-Category: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('sub_cat_id',((isset($child_cats)) ?  $child_cats : []),@$product_data->sub_cat_id,['id' => 'sub_cat_id','class' => 'form-control form-control-sm select2','placeholder' => '--Select Any One--']) }}
                                    {{--name,value,class--}}
                                    @error('sub_cat_id')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('price','Price (NPR.): ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::number('price',@$product_data->price ,['class' => 'form-control'.($errors->has('price') ? 'is-invalid': ''),'id' => 'price' ,'required' => true,'placeholder' => 'Enter Price', "min" => "10"]) }}
                                    {{--name,value,class--}}
                                    @error('price')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('discount','Discount(%): ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::number('discount',@$product_data->discount ,['class' => 'form-control'.($errors->has('discount') ? 'is-invalid': ''),'id' => 'discount' ,'required' => false,'placeholder' => 'Enter Discount', "min" => "0","max" => "95"]) }}
                                    {{--name,value,class--}}
                                    @error('discount')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('stock',' Stock: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::number('stock',@$product_data->stock ,['class' => 'form-control'.($errors->has('stock') ? 'is-invalid': ''),'id' => 'stock' ,'required' => false,'placeholder' => 'Enter Stock', "min" => "0"]) }}
                                    {{--name,value,class--}}
                                    @error('stock')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('brand',' Brand: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::text('brand',@$product_data->brand ,['class' => 'form-control'.($errors->has('brand') ? 'is-invalid': ''),'id' => 'brand' ,'required' => false,'placeholder' => 'Enter Brand']) }}
                                    {{--name,value,class--}}
                                    @error('brand')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('is_featured',' Featured Product: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::checkbox('is_featured',1,@$product_data->is_featured ,[($errors->has('is_featured') ? 'is-invalid': ''),'id' => 'is_featured']) }} Yes
                                    @error('is_featured')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('vendor_id','Vendor: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('vendor_id',$vendor_data,@$product_data->vendor_id,['id' => 'vendor_id','placeholder'=> '---Select One Vendor ---','class' => 'form-control form-control-sm select2']) }}
                                    {{--name,value,class--}}
                                    @error('vendor_id')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>



                            <div class="form-group row">
                                {{ Form::label('status','Status: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('status',['active' => 'Active','inactive' => 'Inactive'],@$product_data->status,['class' => 'form-control'.($errors->has('status') ? 'is-invalid': ''),'id' => 'status' ,'required' => true]) }}
                                    @error('status')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>



{{--                        <div class="form-group row">--}}
{{--                            {{ Form::label('image','Image: ',['class' => 'col-sm-3']) }}--}}
{{--                            <div class="col-sm-4">--}}
{{--                                {{ Form::file('image',['class' => 'form-control-file'.($errors->has('image') ? 'is-invalid': ''),'id' => 'image' ,'required' => (isset($product_data) ? false : true),'accept' => 'image/*']) }}--}}
{{--                                --}}{{--name,value,class--}}
{{--                                @error('image')--}}
{{--                                <p class="invalid-feedback">--}}
{{--                                    {{ $message }}--}}
{{--                                </p>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-4">--}}
{{--                                @if(isset($product_data) && $product_data->image != null && file_exists(public_path().'/uploads/category/Thumb-'.$product_data->image))--}}
{{--                                    <img src="{{ asset('uploads/category/Thumb-' .$product_data->image) }}" alt="image" style="max-width: 150px;" class="img img-thumbnail img-fluid">--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

                            <div class="form-group row">
                                {{ Form::label('related_images','Related Images :', ['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                         <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-success lfm">
                                             <span class="text-white"><i class="fa fa-picture-o"></i> Choose</span>
                                         </a>
                                       </span>
                                        <input id="thumbnail" class="form-control @error('related_images') is-invalid @enderror " type="text" name="related_images" value="{{ @$product_data->related_images }}">
                                         @error('related_images')
                                            <span class="invalid-feedback">
                                        {{ $message }}
                                                @enderror
                                    </span>
                                    </div>
                                    <div class="row">
                                        <div id="holder" style="margin-top:15px;">
                                            @if(isset($product_data,$product_data->related_images))
                                                @php
                                                $images = explode(",",$product_data->related_images);
                                                @endphp
                                                @foreach($images as $product_images)
                                                    <div class="lfm-image col-sm-3">
                                                        <img src="{{asset($product_images)}}" class="img img-responsive img-thumbnail" style="padding-left:20px; margin-right: 10px;"><a href="javascript:;" class="lfm-img-close-btn" onclick="deleteThis(this)" data-value="{{ $product_images }}">X</a>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                        <div style="clear:both"></div>

                                </div>
                            </div>
                        <div class="form-group row">
                            {{ Form::label('','',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btn btn-danger', 'type' => 'reset']) }}
                                {{ Form::button('<i class="fa fa-send"></i> Submit',['class' => 'btn btn-success', 'type' => 'submit']) }}
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
        <script src="{{ asset('assets/admin/js/select2.min.js') }}"></script>
        <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
        <script>
            var options = {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            };
        </script>
        <script>
            CKEDITOR.replace('description',options);
        </script>
        <script>
        $('.select2').select2({ width: "100%" });

        $('#is_parent').change(function () {
            var is_checked = $('#is_parent').prop('checked');
            if (is_checked){
                $('#parent_id_div').addClass('d-none');
            }else{
                $('#parent_id_div').removeClass('d-none');
            }
        });

        $('#cat_id').change(function (e) {
            //console.log(this);
            var cat_id = $(this).val();
            //ajax
            $.ajax({
                url: "{{ route('get-child') }}",
                type:"post",
                data: {
                    _token: "{{ csrf_token() }}",
                    cat_id:cat_id
                },
                success: function (response) {
                    if(typeof(response) != 'object'){
                        response = $.parseJSON(response);
                    }
                    var html_option = '<option value="" selected>--Select Any One--</option>';
                    if (response.status){
                        $('#child_cat_div').removeClass('d-none');
                        $.each(response.data,function (key,value) {
                            html_option += "<option value='"+key+"'>"+value+"</option>";
                        });
                    } else{
                        $('#child_cat_div').addClass('d-none');

                    }
                    $('#sub_cat_id').html(html_option);
                }

            });
        });
    </script>
        <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
            <script>
                var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
                $('.lfm').filemanager('image', {prefix: route_prefix});
            </script>
@endsection
