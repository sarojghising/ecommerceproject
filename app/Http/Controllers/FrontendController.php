<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
class FrontendController extends Controller
{
    protected $product = null;
    protected  $category = null;
    public function __construct(Product $product,Category $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    public function index(){
        $parent_cats = $this->category->getAllParents();
        $this->product = $this->product->where('is_featured',1)->where('status','active')->orderBy('id','DESC')->paginate(24);
        return view('home.index')
            ->with('categories',$parent_cats)
            ->with('all_products', $this->product);
    }
}
