<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page_data = array(
            array(
                'title' => 'About Us',
                'slug' => 'about-us',
                'summary' => 'This is test Summary',
            ),
            array(
              'title' => 'Terms and Condition',
              'slug' => 'terms-and-condition',
              'summary' =>   'This is test Summary'
            ),
            array(
                'title' => 'Help and FAQ',
                'slug' => 'help-and-faq',
                'summary' =>   'This is test Summary'
            ),
            array(
                'title' => 'CSR',
                'slug' => 'scr',
                'summary' =>   'This is test Summary'
            ),
            array(
                'title' => 'Return Policy',
                'slug' => 'return-policy',
                'summary' =>   'This is test Summary'
            ),
            array(
                'title' => 'Delivery Policy',
                'slug' => 'delivery-policy',
                'summary' =>   'This is test Summary'
            ),
        );

        DB::table('pages')->insert($page_data);
    }
}
