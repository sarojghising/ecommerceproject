@extends('admin.layouts.admin')
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Pages{{ isset($page_data) ? 'Update' : 'Add' }}</div>
                    </div>
                    <div class="ibox-body">
                        @if(isset($page_data))
                            {{ Form::open(['url' => route('pages.update',$page_data->id),'class' => 'form', 'id' => 'pages_add', 'files' => true,'method' => 'patch']) }}
                        @endif
                        <div class="form-group row">
                            {{ Form::label('title','Title: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::text('title',@$page_data->title ,['class' => 'form-control'.($errors->has('title') ? 'is-invalid': ''),'id' => 'title' ,'required' => true,'placeholder' => 'Enter Title']) }}
                                {{--name,value,class--}}
                                @error('title')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('summary','Summary: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::textarea('summary',@$page_data->summary,['class' => 'form-control'.($errors->has('summary') ? 'is-invalid': ''),'id' => 'summary' ,'required' => false,'placeholder' => 'Enter summary','rows' => 5, 'style' => 'resize:none']) }}
                                {{--name,value,class--}}
                                @error('summary')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('description','Description: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::textarea('description',@$page_data->description,['class' => 'form-control'.($errors->has('description') ? 'is-invalid': ''),'id' => 'description' ,'placeholder' => 'Enter description','rows' => 5, 'style' => 'resize:none']) }}
                                {{--name,value,class--}}
                                @error('description')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>

                            <div class="form-group row">
                                {{ Form::label('image','Image: ',['class' => 'col-sm-3']) }}
                                    <div class="col-sm-4">
                                        {{ Form::file('image',['class' => 'form-control-file'.($errors->has('image') ? 'is-invalid': ''),'id' => 'image' ,'required' => (isset($page_data) ? false : true),'accept' => 'image/*']) }}
                                        @error('image')
                                        <p class="invalid-feedback">
                                            {{ $message }}
                                        </p>
                                        @enderror
                                    </div>
                                <div class="col-sm-4">
                                    @if(isset($page_data) && $page_data->image != null && file_exists(public_path().'/uploads/pages/Thumb-'.$page_data->image))
                                        <img src="{{ asset('uploads/pages/Thumb-' .$page_data->image) }}" alt="image" style="max-width: 150px;" class="img img-thumbnail img-fluid">
                                    @endif
                                </div>
                            </div>

                        <div class="form-group row">
                            {{ Form::label('','',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btn btn-danger', 'type' => 'reset']) }}
                                {{ Form::button('<i class="fa fa-send"></i> Submit',['class' => 'btn btn-success', 'type' => 'submit']) }}
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
    </script>
    <script>
        CKEDITOR.replace('description',options);
    </script>
    <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script>
        var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
        $('.lfm').filemanager('image', {prefix: route_prefix});
    </script>
@endsection
