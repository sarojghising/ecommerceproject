@extends('admin.layouts.admin')
@section('content')
@section('styles')
    {{--        <link rel="stylesheet" href="{{ asset('assets/admin/css/datatable.min.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Order List</div>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped table-hover" id="table_id">
                            <thead>
                            <tr>
                                <th>S.NO</th>
                                <th>category_id</th>
                                <th>sub_total</th>
                                <th>delivery_charge</th>
                                <th>Total_amount</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($order_data)
                                @foreach($order_data as $key => $value)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>
                                            <a href="{{ route('all-product-list') }}" target="_blank"> {{ $value->cart_id }}</a>
                                        </td>
                                        <td>{{ $value->sub_total }}</td>
                                        <td>{{ $value->delivery_charge }}</td>
                                        <td>{{ $value->total_amount }}</td>
                                        <td>{{ ucfirst($value->status) }}</td>
                                        <td>
                                            <a href="{{ route('order.edit',$value->id) }}" class="btn btn-success" style="border-radius: 50%">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            {{--delelte data--}}
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    {{--    <script rel="stylesheet" href="{{ asset('assets/admin/js/datatable.min.js') }}"></script>--}}
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection
