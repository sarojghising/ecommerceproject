
<script src="{{ asset('js/manifest.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/home.js') }}"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d491a4fc8925491"></script>
<script>
    $('.parallax100').parallax100();
    $('.add_to_cart').click(function () {
        var product_id = $('.add_to_cart').data('id');
        var total_prod = $('.num-product').val();
        $.ajax({
            url: '/ecommerceproject/public/cart/add',
            type: "get",
            data:{
                "prod_id" :product_id,
                "quantity":total_prod
            },
            success:function (response) {
                if (typeof (response) != 'object'){
                    response = $.parseJSON(response);
                }

                if (response.status){
                    swal(response.msg, "success").then(function () {
                        document.location.href =  document.location.href;
                    });

                }else{
                    swal(response.msg, "error");
                }
            }
        })
    });

    /*---------------------------------------------*/
</script>
@yield('scripts')
</body>
</html>
