@extends('admin.layouts.admin')
@section('content')
@section('styles')
    {{--        <link rel="stylesheet" href="{{ asset('assets/admin/css/datatable.min.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Category List</div>
                        <div class="ibox-tools">
                            <a href="{{ route('category.create') }}"><button class="btn btn-success"><i class="fa fa-plus"></i> Add Category</button></a>
                        </div>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped table-hover" id="table_id">
                            <thead>
                            <tr>
                                <th>S.NO</th>
                                <th>Title</th>
                                <th>Thumbnail</th>
                                <th>slug</th>
                                <th>Is Parent</th>
                                <th>Parent</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($category_list)
                                @foreach($category_list as $key => $value)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->title }}</td>
                                        <td>
                                            @if($value->image != null && file_exists(public_path().'/uploads/category/Thumb-'.$value->image))
                                                <img src="{{ asset('uploads/category/Thumb-'.$value->image) }}" alt="img" style="max-width: 150px" class="img img-responsive img-thumbnail">
                                            @endif
                                        </td>
                                        <td>
                                            {{ $value->slug }}
                                        </td>
                                        <td>
                                            {{ $value->is_parent == 1 ? 'Yes' : 'No' }}
                                        </td>

                                        <td>{{ $value->parent_info['title'] }}</td>
                                        <td>{{ ucfirst($value->status) }}</td>
                                        <td>
                                            <a href="{{ route('category.edit',$value->id) }}" class="btn btn-success" style="border-radius: 50%">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            {{--delelte data--}}
                                            {{ Form::open(['url' => route('category.destroy',$value->id),'class' => 'form' ,'method' =>'delete','onsubmit' =>"return confirm('Are u sure u want to delete this category.?')"]) }}
                                            {{Form::button('<i class="fa fa-trash"></i>',['class' => 'btn btn-danger','style' => 'border-radius:50% ','type' => 'submit'])}}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    {{--    <script rel="stylesheet" href="{{ asset('assets/admin/js/datatable.min.js') }}"></script>--}}
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection
