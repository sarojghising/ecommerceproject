<?php

namespace App\Http\Controllers;

use App\Mail\Notification;
use App\User;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $users = null;

    public function __construct(User $users)
    {
        $this->users = $users;
    }

    public function index()
    {
        $this->users = $this->users->getAllUsers();
        return view('admin.users')
            ->with('users_list', $this->users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->users->getRules();
        $request->validate($rules);
        $data = $request->all();
        $this->users->fill($data);
        $success = $this->users->save();

        if ($success) {
            $request->session()->flash('success', 'users added successfully');
        } else {
            $request->session()->flash('error', 'Sorry ! there was problem while adding users.');
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->users = $this->users->find($id);
        if (!$this->users) {
            request()->session()->flash('error', 'users not found');
            return redirect()->route('users.index');
        }
        return view('admin.users-form')->with('users_data', $this->users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->users->getRules('update');
        $request->validate($rules);
        $data = $request->all();

        // catch id of banner to update the data
        $this->users = $this->users->find($id);
        if (!$this->users) {
            request()->session()->flash('error', 'users id not found');
            return redirect()->route('users.index');
        }

        // fill and save session message and redirect the route
        $this->users->fill($data);
        $success = $this->users->save();
        if ($success) {
            $request->session()->flash('success', 'Updated Successfully');
        } else {
            $request->session()->flash('error', 'There was problem while Updating.');

        }
        return redirect()->route('users.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->users = $this->users->find($id);

        if (!$this->users) {
            request()->session()->flash('error', 'users id not found');
            return redirect()->route('users.index');
        }

        $success = $this->users->delete();
        if ($success) {
            request()->session()->flash('success', 'users Deleted Successfully');
        } else {
            request()->session()->flash('error', 'Problem deleting users.');

        }
        return redirect()->route('users.index');
    }

    public function submitUser(Request $request)
    {

        $rules = array(
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required|in:vendor,customer'
        );

        $request->validate($rules);
        $data = $request->all();
        $data['email_token'] = \Str::random(100);
        $data['status'] = 'unverified';
        //dd($data);
        $data['password'] = hash::make('password');
        $data['password_confirmation'] = hash::make('password_confirmation');
        $this->users = $this->users->fill($data);
        $success = $this->users->save();
        if ($success) {
            Mail::to('sarojghising13@gmail.com')->send(new Notification($this->users->id, $this->users->email_token));
            $request->session()->flash('status', 'Thank u for registration your account has been created , Please login further Process.');
            return redirect()->route('login');
        } else {
            $request->session()->flash('error', 'Sorry! Your account Could not be created at this time. Please contact our administration');
            return redirect()->route('user-register');
        }
    }

    public function activeUser(Request $request)
    {
        $this->users = $this->users->where('id', $request->user_id)->where('email_token', $request->token)->first();
        //Ådd($this->users);
        if ($this->users){
            $this->users->where('status',$this->users->status)->update(['status'=> 'verified']);
            $request->session()->flash('success', 'your email is verified');
            return redirect()->route('login');
        }else{
            $request->session()->flash('error', 'Sorry! Your account Could not be created at this time. Please contact our administration');
            return redirect()->route('user-register');
        }
    }
}
