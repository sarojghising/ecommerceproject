<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Image;
use App\Models\Banner;
class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected  $banner = null;
    public function __construct(Banner $banner)
    {
            $this->banner = $banner;
    }


    public function index()
    {
        $this->banner = $this->banner->orderBy('id','DESC')->get();
        return view('admin.banner')->with('banner_list',$this->banner);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = $this->banner->getRules();
        $request->validate($rules);
        $data = $request->all();
        if ($request->has(('image'))){
            $banner_image = uploadImage($request->image,'banner','1920x930');
            if ($banner_image){
                $data['image'] = $banner_image;
            }
        }
        $this->banner->fill($data);
        $success =  $this->banner->save();

        if ($success){
            $request->session()->flash('success','Banner added successfully');
        }else{
            $request->session()->flash('error','Sorry ! there was problem while adding banner.');
        }

        return redirect()->route('banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    public function edit($id)
    {
        $this->banner = $this->banner->find($id);
        if (!$this->banner){
            request()->session()->flash('error','Banner not found');
            return redirect()->route('banner.index');
        }

        return view('admin.banner-form')->with('banner_data',$this->banner);

    }
    public function update(Request $request, $id)
    {
        // rule of banner
        $rules = $this->banner->getRules('update');
        $request->validate($rules);
        $data = $request->all();

        // catch id of banner to update the data
        $this->banner = $this->banner->find($id);
        if(!$this->banner){
            request()->session()->flash('error','Banner id not found');
            return redirect()->route('banner.index');
        }

        // new image uploads
        if ($request->has(('image'))){
            $banner_image = uploadImage($request->image,'banner','1920x930');
            if ($banner_image){
                $data['image'] = $banner_image;

                //old image delete in serve and db
                if (file_exists(public_path().'/uploads/banner/'.$this->banner->image))
                {
                    unlink(public_path().'/uploads/banner/'.$this->banner->image);
                    unlink(public_path().'/uploads/banner/Thumb-'.$this->banner->image);
                }
            }

        }

        // fill and save session message and redirect the route
        $this->banner->fill($data);
        $success = $this->banner->save();
        if ($success){
            $request->session()->flash('success','Updated Successfully');
        }else{
            $request->session()->flash('error','There was problem while Updating.');

        }
        return redirect()->route('banner.index');
    }


    public function destroy($id)
    {

        /// recieve data  from id

        $this->banner = $this->banner->find($id);

        if(!$this->banner){
            request()->session()->flash('error','Banner id not found');
            return redirect()->route('banner.index');
        }

        $image = $this->banner->image;
        $success = $this->banner->delete();
        if ($success){
            if($image != null && file_exists(public_path().'/uploads/banner/'.$image)){
                unlink(public_path().'/uploads/banner/'.$image);
                unlink(public_path().'/uploads/banner/Thumb-'.$image);
            }
            request()->session()->flash('success','Banner Deleted Successfully');
        }else{
            request()->session()->flash('error','Problem deleting banner.');

        }
        return redirect()->route('banner.index');
    }
}
