@extends('layouts.master')
@section('title',$product_details->title.'|Bcom, an online store')
@section('meta')
    <meta name="keyword" content="online shop, ecommerce, nepali ecommerce, online shopping, shopping cart, kathmandu, Mt.Everest, everest">
    <meta name="description" content="First online nepali ecommerce portal providing you everthing you need">
    <meta name="author" content="Admin Bcom">

    <meta property="og:url" content="{{ route('products-show',$product_details->slug ) }}">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{{ $product_details->title }}">
    <meta property="og:description" content="{{ $product_details->summary }}">
    <meta property="og:image" content="{{ asset($product_details->image ) }}">
@endsection

@section('main-content')
    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="{{ route('landing') }}" class="stext-109 cl8 hov-cl1 trans-04">
                Home
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>
            <a href="{{ route('parent-category-product',$product_details->cat_info->slug ) }}" class="stext-109 cl8 hov-cl1 trans-04">
                {{ $product_details->cat_info['title'] }}
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>
            @if($product_details->sub_cat_info)
                <a href="{{ route('child-category-product',[$product_details->cat_info['slug'],$product_details->sub_cat_info->slug ]) }}" class="stext-109 cl8 hov-cl1 trans-04">
                    {{ $product_details->sub_cat_info['title'] }}
                    <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                </a>
            @endif

            <span class="stext-109 cl4">
				{{ $product_details->title }}
			</span>
        </div>
    </div>

    <!-- Product Detail -->
    <section class="sec-product-detail bg0 p-t-65 p-b-60">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-7 p-b-30">
                    <div class="p-l-25 p-r-30 p-lr-0-lg">
                        <div class="wrap-slick3 flex-sb flex-w">
                            <div class="wrap-slick3-dots"></div>
                            <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                            <div class="slick3 gallery-lb">
                                @if($product_details->related_images)
                                    @php
                                            $all_images = explode(',',$product_details->related_images);
                                    @endphp
                                    @foreach($all_images as $images)
                                    <div class="item-slick3" data-thumb="{{ asset($images) }}">
                                            <div class="wrap-pic-w pos-relative">
                                                <img src="{{ asset($images) }}" alt="{{ $product_details->title }}">

                                                <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="{{ asset($images) }}">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>
                                     </div>
                                    @endforeach
                                @else
                                    <div class="item-slick3" data-thumb="{{ asset($product_details->image) }}">
                                    <div class="wrap-pic-w pos-relative">
                                        <img src="{{ asset($product_details->image) }}" alt="{{ $product_details->title }}">

                                        <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="{{ asset($product_details->image) }}">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-5 p-b-30">
                    <div class="p-r-50 p-t-5 p-lr-0-lg">
                        <h4 class="mtext-105 cl2 js-name-detail p-b-14">
                            {{ $product_details->title }}
                        </h4>

                        <span class="mtext-106 cl2">
                            @php
                                    $cost = $product_details->price;
                                    if ($product_details->discount > 0 ){
                                            $cost = ($cost - (($cost*$product_details->discount)/100));
                                    }
                            @endphp
                            NPR. {{ number_format($cost) }}
                            @if($product_details->discount > 0)
                                <del style="color: #ff872f;">{{ number_format($product_details->price) }}</del>
                            @endif
						</span>

                        <p class="stext-102 cl3 p-t-23">
                            {{ $product_details->summary }}
                        </p>

                        <!--  -->
                        <div class="p-t-33">

                            <div class="flex-w flex-r-m p-b-10">
                                <div class="size-204 flex-w flex-m respon6-next">
                                    <div class="wrap-num-product flex-w m-r-20 m-tb-10">
                                        <div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
                                            <i class="fs-16 zmdi zmdi-minus"></i>
                                        </div>

                                        <input class="mtext-104 cl3 txt-center num-product" type="number" name="num-product" value="{{ $cart_count }}">

                                        <div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
                                            <i class="fs-16 zmdi zmdi-plus"></i>
                                        </div>
                                    </div>

                                    <button  data-id =  {{ $product_details->id }} , class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail add_to_cart">
                                        Add to cart
                                    </button>
                                </div>
                            </div>
                        </div>

                        <!--  -->
                        <div class="flex-w flex-m p-l-100 p-t-40 respon7">
                            <div class="addthis_inline_share_toolbox"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bor10 m-t-50 p-t-43 p-b-40">
                <!-- Tab01 -->
                <div class="tab01">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item p-b-10">
                            <a class="nav-link active" data-toggle="tab" href="#description" role="tab">Description</a>
                        </li>



                        <li class="nav-item p-b-10">
                            <a class="nav-link" data-toggle="tab" href="#reviews" role="tab">Reviews ({{ $product_details->reviews->count() }})</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content p-t-43">
                        <!-- - -->
                        <div class="tab-pane fade show active" id="description" role="tabpanel">
                            <div class="how-pos2 p-lr-15-md">
                                <p class="stext-102 cl6">
                                    {!! html_entity_decode($product_details->description) !!}
                                </p>
                            </div>
                        </div>

                        <!-- - -->
                        <div class="tab-pane fade" id="reviews" role="tabpanel">
                            <div class="row">
                                <div class="col-sm-10 col-md-8 col-lg-6 m-lr-auto">
                                    <div class="p-b-30 m-lr-15-sm">
                                        @if($product_details->reviews)
                                            <!-- Review -->
                                            @foreach($product_details->reviews as $review_info)
{{--                                                    {{ $review_info }}--}}
                                                <div class="flex-w flex-t p-b-68">
                                                    <div class="wrap-pic-s size-109 bor0 of-hidden m-r-18 m-t-6">
                                                        <img src="{{ ($review_info->user_info['user_infos']['image'] ) != null ? asset($review_info->user_info['user_infos']['image']): asset('images/userinfo.png') }}" alt="AVATAR">
                                                    </div>

                                                    <div class="size-207">
                                                        <div class="flex-w flex-sb-m p-b-17">
													<span class="mtext-107 cl2 p-r-20">
														{{ $review_info->user_info->name }}
													</span>
                                                            <span class="fs-18 cl11">
                                                                @for($i=1;$i<=5; $i++)
{{--                                                                            5 >= 1--}}
                                                                    @if($review_info->rate >= $i)
                                                                        <i class="zmdi zmdi-star"></i>
                                                                    @else
                                                                        <i class="zmdi zmdi-star-outline"></i>
                                                                    @endif
                                                                @endfor
													        </span>
                                                        </div>

                                                        <p class="stext-102 cl6">
                                                            {{ $review_info->review }}
                                                        </p>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                            @auth
                                            <!-- Add review -->
                                                <form class="w-full" method="post" action="{{ route('post-review',$product_details->id) }}">
                                                    @csrf
                                                    @include('admin.sections.notification')
                                                    <h5 class="mtext-108 cl2 p-b-7">
                                                        Add a review
                                                    </h5>

                                                    <p class="stext-102 cl6">
                                                        Your email address will not be published. Required fields are marked *
                                                    </p>

                                                    <div class="flex-w flex-m p-t-50 p-b-23">
												<span class="stext-102 cl3 m-r-16">
													Your Rating
												</span>

                                                        <span class="wrap-rating fs-18 cl11 pointer">
													<i class="item-rating pointer zmdi zmdi-star-outline"></i>
													<i class="item-rating pointer zmdi zmdi-star-outline"></i>
													<i class="item-rating pointer zmdi zmdi-star-outline"></i>
													<i class="item-rating pointer zmdi zmdi-star-outline"></i>
													<i class="item-rating pointer zmdi zmdi-star-outline"></i>
													<input class="dis-none" type="number" name="rating">
												</span>
                                                    </div>

                                                    <div class="row p-b-25">
                                                        <div class="col-12 p-b-5">
                                                            <label class="stext-102 cl3" for="review">Your review</label>
                                                            <textarea class="size-110 bor8 stext-102 cl2 p-lr-20 p-tb-10" id="review" name="review"></textarea>
                                                        </div>

{{--                                                        <div class="col-sm-6 p-b-5">--}}
{{--                                                            <label class="stext-102 cl3" for="name">Name</label>--}}
{{--                                                            <input class="size-111 bor8 stext-102 cl2 p-lr-20" id="name" type="text" name="name">--}}
{{--                                                        </div>--}}

{{--                                                        <div class="col-sm-6 p-b-5">--}}
{{--                                                            <label class="stext-102 cl3" for="email">Email</label>--}}
{{--                                                            <input class="size-111 bor8 stext-102 cl2 p-lr-20" id="email" type="text" name="email">--}}
{{--                                                        </div>--}}
                                                    </div>

                                                    <button class="flex-c-m stext-101 cl0 size-112 bg7 bor11 hov-btn3 p-lr-15 trans-04 m-b-10">
                                                        Submit
                                                    </button>
                                                </form>
                                            @else
                                                Please <a href="{{ route('login') }}">Login </a> or <a href="{{ route('user-register') }}">Register</a> to review this products
                                            @endauth
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Related Products -->
    <section class="sec-relate-product bg0 p-t-45 p-b-105">
        <div class="container">
            <div class="p-b-45">
                <h3 class="ltext-106 cl5 txt-center">
                    Related Products
                </h3>
            </div>

            <!-- Slide2 -->
            <div class="wrap-slick2">
                <div class="slick2">
                    @if($product_details->related_product)
                        @foreach($product_details->related_product as $product_list)
                            @if($product_list->id != $product_details->id)
                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                <!-- Block2 -->
                                <div class="block2">
                                    <div class="block2-pic hov-img0">
                                        <a href="{{ route('products-show',$product_list->slug) }}">
                                            <img src="{{ asset($product_list->image)  }}" alt="IMG-PRODUCT">
                                        </a>

                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                            <a href="{{ route('products-show',$product_list->slug)  }}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                {{ $product_list->title }} <br>
                                                @if($product_list->discount > 0)
                                                    <span style="color: #f1a417"><strong>Discount</strong>: {{ $product_list->discount }} % off </span>
                                                @endif
                                            </a>

                                            <span class="stext-105 cl3">
                                                @php
                                                    $price = $product_list->price;
                                                     if ($product_list->discount > 0){
                                                     $price = ($price - ( ($price * $product_list->discount )/100));
                                                     }
                                                @endphp
                                                NPR.{{ number_format($price) }}
                                                @if($product_list->discount > 0)
                                                    <del style="color:#cb4a4e"> {{ number_format($product_list->discount ) }} %</del>
                                                @endif
                                            </span>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        @endif
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
    <script>
        $('.gallery-lb').each(function() { // the containers for all your galleries
            $(this).magnificPopup({
                delegate: 'a', // the selector for gallery item
                type: 'image',
                gallery: {
                    enabled:true
                },
                mainClass: 'mfp-fade'
            });
        });
    </script>
    @endsection