<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title','summary','description','cat_id','sub_cat_id','price',
        'discount','stock','image','related_images','brand','status','is_featured','vendor_id','slug','added_by'];

    public function  getRules(){
        return [
            'title' => 'required|string',
            'summary' => 'required|string',
            'description' => 'nullable|string',
            'cat_id' => 'required|exists:categories,id',
            'sub_cat_id' => 'nullable|exists:categories,id',
            'price' => 'required|numeric|min:10',
            'discount' => 'nullable|numeric|min:0|max:95',
            'stock' => 'nullable|min:0',
            'related_images' => 'required|string',
            'brand' => 'nullable|string',
            'status' => 'required|in:active,inactive',
            'is_featured' => 'required|in:1',
            'vendor_id' => 'nullable|exists:users,id',
        ];
    }
    public function getSlug($title){
        $slug = \Str::slug($title);
        $exists = $this->where('slug',$slug)->first();
        if ($exists){
            $slug .= date('Ymdhis');
        }
        return $slug;
    }
    public function cat_info(){
        return $this->hasOne('App\Models\Category','id','cat_id');

    }
    public function sub_cat_info(){
        return $this->hasOne('App\Models\Category','id','sub_cat_id');
    }
    public function  getAllProducts(){
        return $this->with(['cat_info','sub_cat_info'])->orderBy('id','DESC')->get();
    }
    public function vendor(){
        return $this->hasOne('App\User','id','vendor_id');
    }
    public function reviews(){
        return $this->hasMany('App\Models\ProductReview','product_id','id')->where('status','active')->with('user_info');

    }
    public  function related_product(){
        return $this->hasMany('App\Models\Product','cat_id','cat_id')->where('status','active')->limit(8);
    }

    public function getProductsBySlug($slug){
            return $this->with(['cat_info','sub_cat_info','vendor','reviews','related_product'])->where('slug',$slug)->where('status','active')->first();
    }
}