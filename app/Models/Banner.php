<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{

    protected $fillable =['title','link','image','status','added_by'];


    function getRules($act = 'add'){

        $rules =  [
            'title'  => ['required','string'],
            'link' => 'required|url',
            'image' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
            'status' => 'required|in:active,inactive'
        ];
        if ($act != 'add'){
            $rules['image'] = 'sometimes|image|mimes:jpeg,jpg,png,gif,svg';
        }
        return $rules;
    }
}
