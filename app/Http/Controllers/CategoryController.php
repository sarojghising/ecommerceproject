<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $category = null;
    function  __construct(Category $_category)
    {
        $this->category = $_category;
    }


    public function getProductByPrentCat(Request $request){
        $this->category = $this->category->getCategoryBySlug($request->slug);
        if (!$this->category){
            abort(404);
        }
        return view('home.category-product')
            ->with('category',$this->category)
            ->with('all_products',$this->category->products);
        //dd($this->category);
    }
    public function getProductByChildCat(Request $request)
    {
        $this->category = $this->category->getChildCategoryBySlug($request->child_slug);
        if (!$this->category){
            abort(404);
        }
        return view('home.category-product')
            ->with('category',$this->category)
            ->with('all_products',$this->category->child_products);
        //dd($this->category);
    }

    public function index()
    {
        $all_categories = $this->category->getAllCategories();
        return view('admin.category')->with('category_list',$all_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->category = $this->category->getAllParents();
        return view('admin.category-form')->with('parent_cats',$this->category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->category->getRules();
         $request->validate($rules);
         $data = $request->all();
         $data['added_by'] = $request->user()->id;
         $data['slug'] = $this->category->getSlug($request->title);

         if ($request->has('image')) {
             $category_image = uploadImage($request->image, 'category', '200x200');
             if ($category_image) {
                 $data['image'] = $category_image;
             }
         }
         $data['is_parent'] = $request->input('is_parent',0);
         $data['parent_id'] = (isset($request->is_parent)) ? null : $request->parent_id;
         $this->category->fill($data);
         $success = $this->category->save();

         if ($success){
             $request->session()->flash('success','Category added successfully');
         }else{
             $request->session()->flash('error','Sorry ! there was problem while uploading category');
         }
         return  redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->category->find($id);
        if (!$data){
            request()->session()->flash('error','id not found');
            return redirect()->back();
        }
        $this->category = $this->category->getAllParents();
        return view('admin.category-form')->with('category_data',$data)->with('parent_cats',$this->category);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->category = $this->category->find($id);
        if (!$this->category){
            $request->session()->flash('error','category id not found');
            return redirect()->back();
        }

        $rules = $this->category->getRules('update');
        $request->validate($rules);
        $data = $request->all();

        if ($request->has('image')) {
            $category_image = uploadImage($request->image, 'category', '200x200');
            if ($category_image) {
                $data['image'] = $category_image;
            }
        }

        $data['is_parent'] = $request->input('is_parent',0);
        $data['parent_id'] = (isset($request->is_parent)) ? null : $request->parent_id;

        $old_parent = $this->category->is_parent;
        $this->category->fill($data);
        $success = $this->category->save();

        if ($success){
            //Update categories SET parent_id = $data['parent_id'] where parent_id = $this->category->id;
            if ($old_parent){
                $this->category->where('parent_id',$this->category->id)->update(['parent_id' => $data['parent_id']]);

            }
            $request->session()->flash('success','Category Update successfully');
        }else{
            $request->session()->flash('error','Sorry ! there was problem while Update category');
        }
        return  redirect()->route('category.index');
    }

    public function  getChildCats(Request $request){
        // json return
        $data  = $this->category->where('parent_id', $request->cat_id)->pluck('title','id');
        if ($data->count()){
            return response()->json(['status' => true , 'data' => $data]);
        }else{
            return response()->json(['status' => false , 'data' => null]);
        }
    }
    public function destroy($id)
    {
        $this->category = $this->category->find($id);
        if (!$this->category){
            request()->session()->flash('error','id not found');
            return redirect()->back();
        }

        $image = $this->category->image;

        $child_id = $this->category->getChildCatId($id);
        //select * from category where is_parent = 1 and id = parent_id
        //  1 => 3 , 1 => 4

        $del = $this->category->delete();
        if ($del){
            //UPDATE categories SET is_parent = 1 WHERE id = 2 OR Id =3
            //UPDATE categories SET is_parent = 1 WHERE id IN (3,4);
            //child to parent shift

            if ($child_id->count() > 0){
                $this->category->whereIn('id',$child_id)->update(['is_parent' => 1]);
            }

            if (!empty($image) && file_exists(public_path().'/uploads/category/'.$image)){
                unlink(public_path().'/uploads/category/'.$image);
                unlink(public_path().'/uploads/category/Thumb-'.$image);
            }

         request()->session()->flash('success','Category added successfully');
        }else{
         request()->session()->flash('error','Sorry There was problem while deleting category');
        }

        return redirect()->route('category.index');





    }


}
