@extends('layouts.master')
@section('title','Shop Page |Bcom, an online store')
@section('meta')
    <meta name="keyword" content="online shop, ecommerce, nepali ecommerce, online shopping, shopping cart, kathmandu, Mt.Everest, everest">
    <meta name="description" content="First online nepali ecommerce portal providing you everthing you need">
    <meta name="author" content="Admin Bcom">

    <meta property="og:url" content="{{ route('all-product-list') }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Home Page |Bcom, an online store">
    <meta property="og:description" content="First online nepali ecommerce portal providing you everthing you need">
    <meta property="og:image" content="{{ asset('images/icons/logo-01.png') }}">
@endsection

@section('main-content')
    <!-- Product -->
    <section class="bg0 p-t-23 p-b-140">
        <div class="container">
            <div class="p-b-10">
                <h3 class="ltext-103 cl5">
                    {{ $category->title }} Products
                </h3>
            </div>


            @if($all_products->count())
                @include('home.section.product-list')
                @else
                {!! "<p class='alert alert-danger'>This category doesn't have any products</p>" !!}
            @endif
        </div>
    </section>
@endsection

