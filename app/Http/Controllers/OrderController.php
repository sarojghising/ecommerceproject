<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $order = null;
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index()
    {
        $this->order = $this->order->orderBy('id','DESC')->get();
        return view('admin.order-table')->with('order_data',$this->order);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.order-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->order = $this->order->find($id);
        if (!$this->order){
            request()->session()->flash('error','id not found');
        }
        return view('admin.order-form')->with('order_list',$this->order);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $this->order = $this->order->find($id);
        if (!$this->order){
            request()->session()->flash('error','id not found');
            return redirect()->route('order.index');
        }
        $this->order->where('status',$this->order->status)->update(['status' =>'verified']);
        $this->order->fill($data);
        $success = $this->order->save();
        if ($success){
            $request->session()->flash('success','Order Update Successfully');
            return redirect()->route('order.index');
        }else{
            $request->session()->flash('error','Order not Updated');
            return redirect()->route('order.index');
        }




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
