<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role','status','phone','email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getAllVendors(){
        return $this->where('role','vendor')->pluck('name','id');
    }

    public function getRules($act = 'add')
    {
        $update = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'status' => 'required|in:verified,inverified',
        ];

        if ($act != 'add') {
            $update['email'] = 'required|email';
    }
        return $update;
    }


    public function getAllUsers(){
        return $this->orderBy('id','DESC')->get();
    }
    public  function user_infos(){
        return $this->hasOne('App\Models\UserInfo','user_id','id');
    }


}
