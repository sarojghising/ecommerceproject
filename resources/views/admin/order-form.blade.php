@extends('admin.layouts.admin')
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Order {{ isset($order_list) ? 'Update' : 'Add' }}</div>
                    </div>
                    <div class="ibox-body">
                        @if(isset($order_list))
                            {{ Form::open(['url' => route('order.update',$order_list->id),'class' => 'form', 'id' => 'banner_add','method' => 'patch']) }}
                        @endif
                        <div class="form-group row">
                            {{ Form::label('sub_total','Sub_Total: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::number('sub_total',@$order_list->sub_total ,['class' => 'form-control'.($errors->has('title') ? 'is-invalid': ''),'id' => 'sub_total' ,'required' => isset($order_list) ? false : true,'placeholder' => 'Enter Sub_total']) }}
                                {{--name,value,class--}}
                                @error('sub_total')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>
                            {{ $errors }}

                        <div class="form-group row">
                            {{ Form::label('delivery_charge','Delivery_Charge: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::number('delivery_charge',@$order_list->delivery_charge,['class' => 'form-control'.($errors->has('delivery_charge') ? 'is-invalid': ''),'id' => 'delivery_charge' ,'required' => isset($order_list) ? false : true,'placeholder' => 'Enter Delivery_Charge']) }}
                                {{--name,value,class--}}
                                @error('delivery_charge')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            {{ Form::label('status','Status: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::select('status',['new' => 'New','verified' => 'Verified','delivered' => 'Delivered','processing' => 'Processing'],@$order_list->status,['class' => 'form-control'.($errors->has('status') ? 'is-invalid': ''),'id' => 'status' ,'required' => isset($order_list) ? false : true]) }}

                                @error('status')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>
                            <div class="form-group row">
                                {{ Form::label('total_amount','Total_Amount: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::number('total_amount',@$order_list->total_amount,['class' => 'form-control'.($errors->has('total_amount') ? 'is-invalid': ''),'id' => 'total_amount' ,'required' => isset($order_list) ? false : true,'placeholder' => 'Enter Total_amount']) }}
                                    {{--name,value,class--}}
                                    @error('total_amount')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>

                        <div class="form-group row">
                            {{ Form::label('button','',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btn btn-danger', 'type' => 'reset']) }}
                                {{ Form::button('<i class="fa fa-send"></i> Submit',['class' => 'btn btn-success', 'type' => 'submit']) }}
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
