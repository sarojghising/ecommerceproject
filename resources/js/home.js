global.$ = global.jQuery = require('jquery');
require('../frontend/vendor/animsition/js/animsition');
require('../frontend/vendor/slick/slick');
require('../frontend/js/slick-custom');
require('../frontend/vendor/parallax100/parallax100');
require('../frontend/vendor/isotope/isotope.pkgd.min');
require('../frontend/vendor/sweetalert/sweetalert.min');
require('../frontend/vendor/MagnificPopup/jquery.magnific-popup.min.js');
require('../frontend/vendor/perfect-scrollbar/perfect-scrollbar.min');
require('../frontend/js/main');






