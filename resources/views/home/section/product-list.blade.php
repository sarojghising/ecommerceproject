<div class="row isotope-grid">
    @if(isset($all_products))
        @foreach($all_products as $product_list)
        <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item {{ $product_list->cat_id }}">
        <!-- Block2 -->
        <div class="block2">
            <div class="block2-pic hov-img0">
                <a href="{{ route('products-show',$product_list->slug) }}">
                    <img src="{{ asset($product_list->image)  }}" alt="IMG-PRODUCT">
                </a>

            </div>

            <div class="block2-txt flex-w flex-t p-t-14">
                <div class="block2-txt-child1 flex-col-l ">
                    <a href="{{ route('products-show',$product_list->slug)  }}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                        {{ $product_list->title }} <br>
                        @if($product_list->discount > 0)
                            <span style="color: #f1a417"><strong>Discount</strong>: {{ $product_list->discount }} % off </span>
                        @endif
                    </a>

                    <span class="stext-105 cl3">
                        @php
                           $price = $product_list->price;
                            if ($product_list->discount > 0){
                            $price = ($price - ( ($price * $product_list->discount )/100));
                            }
                        @endphp
                        NPR.{{ number_format($price) }}
                        @if($product_list->discount > 0)
                            <del style="color:#cb4a4e"> {{ number_format($product_list->discount ) }} %</del>
                        @endif
                    </span>
                </div>


            </div>
        </div>
    </div>
        @endforeach
    @endif
</div>

