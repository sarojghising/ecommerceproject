@extends('admin.layouts.admin')
@section('content')
@section('styles')
    {{--        <link rel="stylesheet" href="{{ asset('assets/admin/css/datatable.min.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Banner List</div>
                        <div class="ibox-tools">
                            <a href="{{ route('banner.create') }}"><button class="btn btn-success"><i class="fa fa-plus"></i> Add Banner</button></a>
                        </div>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped table-hover" id="table_id">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Thumbnail</th>
                                    <th>Link</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($banner_list)
                                @foreach($banner_list as $key => $value)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>
                                                @if($value->image != null && file_exists(public_path().'/uploads/banner/Thumb-'.$value->image))
                                                    <img src="{{ asset('uploads/banner/Thumb-'.$value->image) }}" alt="img" style="max-width: 150px" class="img img-responsive img-thumbnail">
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ $value->link }}" target="_blank"> {{ $value->link }}</a>
                                            </td>
                                            <td>{{ ucfirst($value->status) }}</td>
                                            <td>
                                                <a href="{{ route('banner.edit',$value->id) }}" class="btn btn-success" style="border-radius: 50%">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                {{--delelte data--}}
                                                {{ Form::open(['url' => route('banner.destroy',$value->id),'class' => 'form' ,'method' =>'delete','onsubmit' =>"return confirm('Are u sure u want to delete this banner.?')"]) }}
                                                {{Form::button('<i class="fa fa-trash"></i>',['class' => 'btn btn-danger','style' => 'border-radius:50% ','type' => 'submit'])}}
                                                {{ Form::close() }}
                                            </td>
                                        </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    {{--    <script rel="stylesheet" href="{{ asset('assets/admin/js/datatable.min.js') }}"></script>--}}
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection
