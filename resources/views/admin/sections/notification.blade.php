@if(session('success'))
<div class="alert alert-success alert-bordered">
    {{session('success')}}
</div>
@endif
@if(session('error'))
    <div class="alert alert-error alert-bordered">
        {{session('error')}}
    </div>
@endif

@if(session('status'))
    <div class="alert alert-success alert-bordered">
        {{session('status')}}
    </div>
@endif


@if(session('warning'))
    <div class="alert alert-warning alert-bordered">
        {{session('warning')}}
    </div>
@endif