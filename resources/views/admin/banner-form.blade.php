@extends('admin.layouts.admin')
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Banner {{ isset($banner_data) ? 'Update' : 'Add' }}</div>
                    </div>
                    <div class="ibox-body">
                        @if(isset($banner_data))
                                {{ Form::open(['url' => route('banner.update',$banner_data->id),'class' => 'form', 'id' => 'banner_add', 'files' => true,'method' => 'patch']) }}
                            @else
                                {{ Form::open(['url' => route('banner.store'),'class' => 'form', 'id' => 'banner_add', 'files' => true ]) }}

                            @endif
                                <div class="form-group row">
                                    {{ Form::label('title','Title: ',['class' => 'col-sm-3']) }}
                                    <div class="col-sm-9">
                                        {{ Form::text('title',@$banner_data->title ,['class' => 'form-control'.($errors->has('title') ? 'is-invalid': ''),'id' => 'title' ,'required' => true,'placeholder' => 'Enter Title']) }}
                                        {{--name,value,class--}}
                                        @error('title')
                                        <p class="invalid-feedback">
                                            {{ $message }}
                                        </p>
                                        @enderror
                                    </div>
                                </div>

                        <div class="form-group row">
                            {{ Form::label('link','Link: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::url('link',@$banner_data->link,['class' => 'form-control'.($errors->has('link') ? 'is-invalid': ''),'id' => 'link' ,'required' => true,'placeholder' => 'Enter Link']) }}
                                {{--name,value,class--}}
                                @error('title')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            {{ Form::label('status','Status: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::select('status',['active' => 'Active','inactive' => 'Inactive'],@$banner_data->status,['class' => 'form-control'.($errors->has('status') ? 'is-invalid': ''),'id' => 'status' ,'required' => true]) }}

                                @error('status')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                            {{ Form::label('image','Image: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-4">
                                {{ Form::file('image',['class' => 'form-control-file'.($errors->has('image') ? 'is-invalid': ''),'id' => 'image' ,'required' => (isset($banner_data) ? false : true),'accept' => 'image/*']) }}
                                {{--name,value,class--}}
                                @error('image')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>

                            <div class="col-sm-4">
                                @if(isset($banner_data) && $banner_data->image != null && file_exists(public_path().'/uploads/banner/Thumb-'.$banner_data->image))
                                    <img src="{{ asset('uploads/banner/Thumb-' .$banner_data->image) }}" alt="image" style="max-width: 150px;" class="img img-thumbnail img-fluid">
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            {{ Form::label('button','',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btn btn-danger', 'type' => 'reset']) }}
                                {{ Form::button('<i class="fa fa-send"></i> Submit',['class' => 'btn btn-success', 'type' => 'submit']) }}
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
