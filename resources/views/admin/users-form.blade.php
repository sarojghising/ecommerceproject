@extends('admin.layouts.admin')
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Users {{ isset($users_data) ? 'Update' : 'Add' }}</div>
                    </div>
                    <div class="ibox-body">
                        @if(isset($users_data))
                            {{ Form::open(['url' => route('users.update',$users_data->id),'class' => 'form', 'id' => 'users_add', 'files' => true,'method' => 'patch']) }}
                        @else
                            {{ Form::open(['url' => route('users.store'),'class' => 'form', 'id' => 'users_add', 'files' => true ]) }}

                        @endif
                        <div class="form-group row">
                            {{ Form::label('name','Name: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::text('name',@$users_data->name ,['class' => 'form-control'.($errors->has('name') ? 'is-invalid': ''),'id' => 'name' ,'required' => true,'placeholder' => 'Enter Name']) }}
                                {{--name,value,class--}}
                                @error('name')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>

                            <div class="form-group row">
                            {{ Form::label('email','Email: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::text('email',@$users_data->email,['class' => 'form-control'.($errors->has('email') ? 'is-invalid': ''),'id' => 'email' ,'required' => true,'placeholder' => 'Enter Email']) }}
                                {{--name,value,class--}}
                                @error('email')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>
                        @if(!isset($users_data))
                            <div class="form-group row">
                                {{ Form::label('number','Password: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::password('password',['class' => 'form-control'.($errors->has('password') ? 'is-invalid': ''),'id' => 'password' ,'required' => true,'placeholder' => 'Enter Password']) }}
                                    {{--name,value,class--}}
                                    @error('password')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>
                                <div class="form-group row">
                                {{ Form::label('password_confirmation','Confirm Password: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::password('password_confirmation',['class' => 'form-control'.($errors->has('password_confirmation') ? 'is-invalid': ''),'id' => 'password_confirmation' ,'required' => true,'placeholder' => 'Enter Password']) }}
                                    {{--name,value,class--}}
                                    @error('password_confirmation')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>
                            @endif
                            <div class="form-group row">
                                {{ Form::label('role','Role: ',['class' => 'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('role',['admin' => 'admin','customer'=> 'customer' , 'vendor'=> 'vendor'],@$users_data->role,['class' => 'form-control'.($errors->has('role') ? 'is-invalid': ''),'id' => 'role' ,'required' => true]) }}

                                    @error('role')
                                    <p class="invalid-feedback">
                                        {{ $message }}
                                    </p>
                                    @enderror
                                </div>
                            </div>


                        <div class="form-group row">
                            {{ Form::label('status','Status: ',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::select('status',['verified' => 'Verified','inverified' => 'Inverified'],@$users_data->status,['class' => 'form-control'.($errors->has('status') ? 'is-invalid': ''),'id' => 'status' ,'required' => true]) }}

                                @error('status')
                                <p class="invalid-feedback">
                                    {{ $message }}
                                </p>
                                @enderror
                            </div>
                        </div>



{{--                        <div class="form-group row">--}}
{{--                            {{ Form::label('image','Image: ',['class' => 'col-sm-3']) }}--}}
{{--                            <div class="col-sm-4">--}}
{{--                                {{ Form::file('image',['class' => 'form-control-file'.($errors->has('image') ? 'is-invalid': ''),'id' => 'image' ,'required' => (isset($users_data) ? false : true),'accept' => 'image/*']) }}--}}
{{--                                --}}{{--name,value,class--}}
{{--                                @error('image')--}}
{{--                                <p class="invalid-feedback">--}}
{{--                                    {{ $message }}--}}
{{--                                </p>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}


                        <div class="form-group row">
                            {{ Form::label('button','',['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btn btn-danger', 'type' => 'reset']) }}
                                {{ Form::button('<i class="fa fa-send"></i> Submit',['class' => 'btn btn-success', 'type' => 'submit']) }}
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection