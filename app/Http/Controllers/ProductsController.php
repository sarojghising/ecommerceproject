<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductReview;
use App\User;
use http\Env\Response;
use Psy\Util\Str;
use Session;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $category = null;
    protected $order = null;
    protected  $users = null;
    protected  $product = null;
    protected  $reveiws = null;
    public function __construct(Category $category, User $user,Product $product,ProductReview $reveiws,Order $order)
    {
        $this->users = $user;
        $this->order = $order;
        $this->category = $category;
        $this->product = $product;
        $this->reveiws = $reveiws;
    }

    public function index()
    {
        $this->product = $this->product->getAllProducts();
        return view('admin.product')->with('products_list',$this->product);
    }

    public function create()
    {
        $parent_category = $this->category->getAllParents();
        $vendor_data = $this->users->getAllVendors();
        return view('admin.product-form')->with('vendor_data',$vendor_data)->with('parents_cats',$parent_category);
    }

    public function store(Request $request)
    {
        $rules = $this->product->getRules();
        $request->validate($rules);
        $data = $request->all();
        $data['slug'] = $this->product->getSlug($data['title']);
        $data['added_by'] = $request->user()->id;
        $data['image'] = explode(',',$request->related_images)[0];
        $data['discount'] = $request->input('discount',0);
        $data['description'] = htmlentities($data['description']);
        $this->product->fill($data);
        $success = $this->product->save();
        if ($success){
             $request->session()->flash('success','Product added Successfully');
        }else{
            $request->session()->flash('error','There was problem while Uploading Product');
        }
        return redirect()->route('products.index');
    }
    public function show($slug)
    {
        $this->product = $this->product->getProductsBySlug($slug);
        if (!$this->product){
            abort(404);
        }
        $current_item_cart =1;

        if (session('_cart')){
            foreach (session('_cart') as $cart_items){
                if ($cart_items['product_id'] == $this->product->id){
                    $current_item_cart = $cart_items['quantity'];
                    break;
                }
            }
        }
        return view('home.product-detail')->with('cart_count',$current_item_cart)
            ->with('product_details',$this->product);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->product = $this->product->find($id);
        $child_cats = $this->category->getChilds($this->product->cat_id);

        if (!$this->product){
            request()->session()->flash('error','product id not found');
            return redirect()->route('products.index');
        }
        $parent_category = $this->category->getAllParents();
        $vendor_data = $this->users->getAllVendors();
        return view('admin.product-form')
            ->with('vendor_data',$vendor_data)
            ->with('child_cats',$child_cats)
            ->with('product_data',$this->product)
            ->with('parents_cats',$parent_category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->product = $this->product->find($id);
        if (!$this->product){
            request()->session()->flash('error','id not found');
            return redirect()->route('products.index');
        }
        $rules = $this->product->getRules();
        $request->validate($rules);
        $data = $request->all();
        $data['added_by'] = $request->user()->id;
        $data['image'] = explode(',',$request->related_images)[0];
        $data['discount'] = $request->input('discount',0);
        $data['description'] = htmlentities($data['description']);
        $this->product->fill($data);
        $success = $this->product->save();
        if ($success){
            $request->session()->flash('success','Product Updated Successfully');
        }else{
            $request->session()->flash('error','There was problem while Updated Product');
        }
        return redirect()->route('products.index');
    }


    public function destroy($id)
    {
        $this->product = $this->product->find($id);
        if (!$this->product){
            request()->session()->flash('error','id not found');
            return redirect()->route('products.index');
        }
        $success = $this->product->delete();
        if ($success){
            request()->session()->flash('success','Product Deleted Successfully');
        }else{
            request()->session()->flash('error','There was problem while deleting product id');
        }

        return redirect()->route('products.index');
    }


    public function getAllProducts(Request $request){
        $parent_cats = $this->category->getAllParents();
        $this->product = $this->product->where('status', 'active')->orderBy('id','DESC')->paginate(32);
        return view('home.product-list')
            ->with('categories',$parent_cats)
            ->with('all_products',$this->product);
    }

    public function submitReview(Request $request){
        if (!empty($request->review) || !empty($request->rating)){
            $data = array(
                'product_id' => $request->product_id,
                'user_id' => $request->user()->id,
                'rate' => $request->input('rating',0),
                'review' => $request->input('review',null),
                'status' =>'active'
            );

            $this->reveiws->fill($data);
            $success = $this->reveiws->save();
            if ($success){
                $request->session()->flash('success','Thank u for your review');
            }else{
                $request->session()->flash('error','Sorry! Your review cannot be submitted at these moment');
            }
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }
    public function addToCart(Request $request){

        $this->product = $this->product->find($request->prod_id);

        if (!$this->product) {
            return response()->json(['status' => false, 'data' => null, 'msg' => 'product not found']);
        }

        $current = array(
          'product_id' => $this->product->id,
          'title' => $this->product->title,
            'image' => asset($this->product->image),
            'link' => route('products-show',$this->product->slug),
            'original_price' => $this->product->price
        );

        $price = $this->product->price;

        if ($this->product->discount > 0){
            $price = ($price - (($price * $this->product->discount)/100));
        }
        $current['price'] = $price;

        //cart calculation cart is handle through session
        $cart = session()->has('_cart') ? session()->get('_cart'): null;
        //quantity
        $quantity = $request->quantity;
        //1
        $total_amount = $price * $quantity; // 1 * 12141.9

        //dd($cart);
        if ($cart){
            //old cart exists
            //dd($cart);

            $index = null;
            foreach ($cart as $cart_index => $cart_items){
                if($this->product->id == $cart_items['product_id']){
                    $index = $cart_index;
                    break;
                }
            }
            //dd($cart_index);
            if ($index !== null){
                //cart product exists gharxa bhana
                $cart[$cart_index]['quantity'] = $quantity;
                $cart[$cart_index]['total_amount'] = $total_amount;
                // to remove cart item  if it is 0

                if ($cart[$cart_index]['quantity'] <= 0){
                    unset($cart[$cart_index]);
                }

            }else{
                // if not card we have to push cart
                $current['quantity'] = $quantity;
                $current['total_amount'] = $total_amount;
                $cart[] = $current;
            }

        }else{
            //
            $current['quantity'] = $quantity;
            $current['total_amount'] = $total_amount;
            $cart[] = $current;
        }

        // dd($cart);
        session()->put('_cart',$cart);
        // dd(session('_cart'));
        //dd($cart);
        return response()->json(['status' => true,'data' => $cart,'msg' =>$this->product->title.'add to Cart']);
        //dd($cart);

        // udpate create delete
    }

    public  function showCart(Request $request){
        return view('home.cart');

    }
    public function checkOut(Request $request){
        $cart = session('_cart');

        if ($cart){

            $cart_id = \Str::random(15);
            //order
            $order_data = array(
                'cart_id' => $cart_id,
                'user_id' => request()->user()->id,
                'delivery_charge' => 150,
                'status' => 'new'
            );
            $sub_total = 0;
            foreach ($cart as $car_items){
                $sub_total += $car_items['total_amount'];
                // cart
                $cart_data = array(
                    'cart_id' => $cart_id,
                    'product_id' => $car_items['product_id'],
                    'user_id' => $request->user()->id,
                    'price' => $car_items['price'],
                    'quantity' => $car_items['quantity'],
                    'total_amount' => $car_items['total_amount']
                );
                $cart = new Cart();
                $cart->fill($cart_data);
                $cart->save();
            }
            $order_data['sub_total'] = $sub_total;
            $order_data['total_amount'] = $sub_total + $order_data['delivery_charge'];
            $order = new Order();
            $order->fill($order_data);
            $status = $order->save();
            if ($status){
                session()->forget('_cart');
                request()->session()->flash('success','Thank u For purchasing from us');
                return redirect()->route(request()->user()->role);
            }else{
                request()->session()->flash('error','Sorry! There was problem while creating order');

            }
            return redirect()->route('cart');

        }else{
            return redirect()->route('all-product-list');
        }

    }
}
